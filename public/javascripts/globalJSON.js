var propTypeJSON = {
	0 : "Not specified",
	1 : "Residential: 1RK",
	2 : "Residential: 1BHK",
	3 : "Residential: 2BHK",
	4 : "Residential: 3BHK & Above",
	5 : "Commercial: Office Space",
	6 : "Commercial: Retail Establishment",
	7 : "Other"
}

var liftServiceJSON = {
	0 : {"dispText" : "Not specified", "ratingValue" : "0"},
	1 : {"dispText" : "Yes, fast/reliable", "ratingValue" : "100"},
	2 : {"dispText" : "Yes, slow/unreliable", "ratingValue" : "55"},
	3 : {"dispText" : "No", "ratingValue" : "10"}
}

var propConditionJSON = {
	0 : "Not specified",
	1 : "New & furnished",
	2 : "New & unfurnished",
	3 : "Renovated & furnished",
	4 : "Renovated & unfurnished",
	5 : "Old & furnished",
	6 : "Old & unfurnished"	
}

var carParkJSON = {
	0 : {"dispText" : "Not specified", "ratingValue" : "0"},
	1 : {"dispText" : "Reserved",  "ratingValue" : "100"},
	2 : {"dispText" : "First come, first served within compound",  "ratingValue" : "70"},
	3 : {"dispText" : "Can usually find space outside compound",  "ratingValue" : "40"},
	4 : {"dispText" : "Rarely get space anywhere", "ratingValue" : "10"}
}

var storesNearbyJSON = {
	0 : {"dispText" : "Not specified", "ratingValue" : "0"}, 
	1 : {"dispText" : "Short walk (< 10 minutes)", "ratingValue" : "100"},
	2 : {"dispText" : "Long walk (> 10 minutes)", "ratingValue" : "70"},
	3 : {"dispText" : "Short drive (< 10 minutes)", "ratingValue" : "40"},
	4 : {"dispText" : "Long drive (> 10 minutes)", "ratingValue" : "10"} 
}

var entertainmentVenuesJSON = {
	0 : {"dispText" : "Not specified", "ratingValue" : "0"},
	1 : {"dispText" : "Many", "ratingValue" : "100"},
	2 : {"dispText" : "A few", "ratingValue" : "55"},
	3 : {"dispText" : "None", "ratingValue" : "10"}
}

var transportConnectivityJSON = {
	0 : {"dispText" : "Not specified", "ratingValue" : "0"},
	1 : {"dispText" : "Bus/MRT/Tram station is within 10 minutes by walk", "ratingValue" : "100"},
	2 : {"dispText" : "Bus/MRT/Tram station is within a 10 minute drive", "ratingValue" : "55"},
	3 : {"dispText" : "Cannot rely on public transport (Taxi or self driving)", "ratingValue" : "10"}
}

var waterSupplyJSON = {	
	1 : {"dispText" : "24 hours", "ratingValue" : "100"},
	2 : {"dispText" : "Most of the day", "ratingValue" : "55"},
	3 : {"dispText" : "Limited hours", "ratingValue" : "10"}
}

var powerSupplyJSON = {	
	1 : {"dispText" : "No power cuts", "ratingValue" : "100"},
	2 : {"dispText" : "Occasional power cuts", "ratingValue" : "55"},
	3 : {"dispText" : "Frequent power cuts", "ratingValue" : "10"}
}

var drainPipesQualityJSON = {	
	1 : {"dispText" : "Excellent", "ratingValue" : "100"},
	2 : {"dispText" : "Good", "ratingValue" : "70"},
	3 : {"dispText" : "Satisfactory", "ratingValue" : "40"},
	4 : {"dispText" : "Can be better", "ratingValue" : "10"}
}

var electricPlugsSwitchesConditionJSON = {	
	1 : {"dispText" : "Excellent", "ratingValue" : "100"},
	2 : {"dispText" : "Good", "ratingValue" : "70"},
	3 : {"dispText" : "Satisfactory", "ratingValue" : "40"},
	4 : {"dispText" : "Can be better", "ratingValue" : "10"}
}

var withinCompoundAmenitiesJSON = {	
	1 : {"dispText" : "Swimming pool", "ratingValue" : "10"},
	2 : {"dispText" : "Gym", "ratingValue" : "25"},
	3 : {"dispText" : "Play area", "ratingValue" : "25"},
	4 : {"dispText" : "Park", "ratingValue" : "40"},
	5 : {"dispText" : "None", "ratingValue" : "0"}
}

var trafficCongestionJSON = {	
	1 : {"dispText" : "Low", "ratingValue" : "100"},
	2 : {"dispText" : "Moderate", "ratingValue" : "55"},
	3 : {"dispText" : "High", "ratingValue" : "10"}
}

var defectsDetectedJSON = {	
	1 : {"dispText" : "No", "ratingValue" : "100"},
	2 : {"dispText" : "Yes, minor/expected", "ratingValue" : "55"},
	3 : {"dispText" : "Yes, major", "ratingValue" : "10"}
}

var ratingWeights = {
	"lift" : "5",
	"carpark" : "8",
	"stores" : "12",
	"entertain" : "6",
	"transport" : "17",
	"watersupp" : "10",
	"powersupp" : "10",
	"drainqual" : "11",
	"eleccond" : "10",
	"incompamen" : "4",
	"traffic" : "6",
	"defects" : "1"
}