//Function for the onclick of the sidebar submenu
$(".menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
// Delete experience link click
$(document).ready(function(){        
    //Call the Ajax method if we are in the MyExperience page and if the user clicks on the delete link.
    var myExpFormExists = $('#experienceList').html();    
    if(myExpFormExists !== undefined){        
        $('.deleteExperienceLink').on('click' , deleteExperience);
    }

    //Call the togglePanel method on click of the collapse button on the Edit Profile page.
    var editProfileFormExists = $('form[name="editUserExpForm"]');
    if(editProfileFormExists.html !== undefined){             
        loadPersonalDetailsSection();       
        $('.panel-heading span.clickable').on('click', togglePanel);
    }

    //Load the existing profile picture if we are in the Upload Photo page.
    var uploadPhotoFormExists = $('form[name="uploadPhotoForm"]');
    if(uploadPhotoFormExists.html() !== undefined){
        $('#crop').on('click', cropImage);
    }
        
    //populate dropdown values if we are in the Edit UserExperience page.
    var editExpFormExists = $('form[name="editUserExpForm"]');    
    if(editExpFormExists.html() !== undefined){
        selectDropdownValues();
    }

    //Display the text for the database values if we are in the Display UserExperience page.
    var dispExpFormExists = $('#dispUserExpTable').html();
    if(dispExpFormExists !== undefined){        
        displayExpValueText();
    }

    //Display the text for the Property type value in the Search Experience Results page.
    var searchExpResultsFormExisits = $('#searchResultsTable').html();
    if(searchExpResultsFormExisits !== undefined){
        displayPropertyTypeText();
    }

    //Change the action of the form based on the button being clicked in Compose message form
    var form = $('form[name="composeMessageForm"]');
    $('#btnSendMessage').on('click', function(){
        $('#composeMessageForm').attr('action','/messages/compose');        
        $('#composeMessageForm').submit();
    });
    $('#btnSaveDraft').on('click', function(){        
        $('#composeMessageForm').attr('action','/messages/saveDraft');
        $('#composeMessageForm').submit();
    });

    //Check if the user is in the Display Messages page.
    var inDisplayMessagesPage = $("#divDisplayMessage").html();
    if(inDisplayMessagesPage !== undefined){
        //Hide the display message div on load of the display messages form.
        $("#divDisplayMessage").hide(); 
        reflectReadStatus();
    }

    //Check if the user is not logged in and call the showlogindialog function.
    if($("#notLoggedIn").val() === 'true'){
        showLoginDialogue();
    }
});

//Function to show differentiation between read and unread messages and update the unread messages count
function reflectReadStatus(){
    //alert("am in reflectReadStatus method");
    /*$('tr[data-messageJSON]').each(function(index){
        var messageThread = $(this);
        alert(messageThread);
    });*/
    //alert($('.messagesRow'));
    $('.messagesRow').each(function(index){
        var messageRow = $(this);
        var messageThread = $(this).data('messagejson');
        var messagesArray = messageThread.messages;
        //alert(messagesArray);
        $.each(messagesArray, function(){
            if(this.readStatus === 'Unread'){
                messageRow.children('td,th').css('background-color','#FFFFFF');
            }
        });
    });
}

//Function to load the reply panel
function loadReply(execCommand){
    var divReplyContent = '';        
    divReplyContent += '<div class="panel panel-info">';
    divReplyContent += '<div class="panel-body">';
    divReplyContent += '<div class="col-xs-12">';
    divReplyContent += '<textarea class="form-control" id="replyBody" name="replyBody" rows="4" cols="150"/>';
    divReplyContent += '<br>';
    divReplyContent += '</div>';    
    divReplyContent += '<div class="col-xs-12">';
    divReplyContent += '<div class="btn-group btn-group-small pull-right">';
    divReplyContent += '<a href="#" class="btn btn-primary btn-small" onClick="invokeController(\'reply\');">Send Reply </a>';
    divReplyContent += '</div>';
    divReplyContent += '</div>';
    divReplyContent += '</div>';    
    $("#divReply").html(divReplyContent);
    $("#divReplyDelete").hide();    
    $("#divReply").show();
}

//Function to invoke the methods for reply/delete/send from drafts
function invokeController(useraction){
    //alert(useraction);
    var messageThreadId = $("#hdnMessageThreadId").val();
    var replyURL = "/messages/reply/" + messageThreadId;
    //var spamURL = "/messages/spam/" + messageThreadId;
    var deleteURL = "/messages/" + messageThreadId;
    var sendFromDraftsURL = "/messages/sendFromDrafts/" + messageThreadId;
    var replyContent = $("#replyBody").val();
    var commonMessageThreadId = $("#hdnCommonMessageThreadId").val();
    var data = {"replyContent" : replyContent, "commonMessageThreadId": commonMessageThreadId};
    if(useraction == 'reply'){
        //alert(replyContent);
        $.ajax({
            type: 'POST',
            url: replyURL,
            data: data            
        }).done(function (response){  
            // Check for a successful (blank) response            
            if (response.msg === '') {                
                location.reload();
            }
            else {
                alert('Error while sending reply: ' + response.msg);
            }          
        });
    }
    if(useraction == 'delete'){
        //alert(replyContent);
        var confirmation = confirm('Are you sure you want to delete this message thread?');

        if(confirmation === true){
            $.ajax({
                type: 'DELETE',
                url: deleteURL 
            }).done(function (response){  
                // Check for a successful (blank) response            
                if (response.msg === '') {                
                    location.reload();
                }
                else {
                    alert('Error while deleting the message: ' + response.msg);
                }          
            });
        }
        else{
            return false;
        }
    }
    if(useraction == 'sendFromDrafts'){
        var confirmation = confirm('Are you sure you want to send this message?');

        if(confirmation === true){
            $.ajax({
                type: 'POST',
                url: sendFromDraftsURL,
                data: data      
            }).done(function (response){  
                // Check for a successful (blank) response            
                if (response.msg === '') {                
                    location.reload();
                }
                else {
                    alert('Error while sending the message: ' + response.msg);
                }          
            });
        }
        else{
            return false;
        }   
    }
}

//Function to call the ajax method to update the message readStatus to 'read'.
function updateMessageReadStatus(messageThread, message){
    //alert(messageThread);
    //alert(message._id);
    var ajaxURL = "/messages/messageStatus/" + message._id;
    $.ajax({
        type: 'POST',
        url: ajaxURL    
    }).done(function (response){  
        // Check for a successful (blank) response            
        if (response.msg === '') {                
            //location.reload();
        }
        else {
            alert('Error while updating the message status: ' + response.msg);
        }          
    });
}

//Function to load the display message div on click of the message.
function loadSelectedMessage(messageJSON){
    var dateSent = new Date(messageJSON.dateModified);
    var divIndMessagesContent = '';    
    $("#refreshInbox").hide();
    $("#paginationIcons").hide();    
    //alert(messageJSON.messageStatus);
    $.each(messageJSON.messages, function(){
        var messageSentDate = new Date(this.dateSent).toUTCString();
        divIndMessagesContent += '<div class="panel panel-info">';
        divIndMessagesContent += '<div class="panel-heading">';
        divIndMessagesContent += '<strong id="messageSenderDetails">'+ this.senderUsername + ' on ' + messageSentDate +'</strong>';
        divIndMessagesContent += '<input type="hidden" name="hdnMessageThreadId" id="hdnMessageThreadId">';        
        //divIndMessagesContent += '<div><span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span></div>';
        divIndMessagesContent += '</div>';        
        divIndMessagesContent += '<div class="panel-body">';
        divIndMessagesContent += '<div class="col-xs-12">';
        divIndMessagesContent += '<div id="messageBody">' + this.content + '</div></div>';        
        divIndMessagesContent += '</div>';
        divIndMessagesContent += '</div>';                
        if(this.readStatus === 'Unread'){
            //alert(this.readStatus);
            updateMessageReadStatus(messageJSON,this);
        }
    });
    divIndMessagesContent += '<div class="col-xs-12" id="divReplyDelete">';
    divIndMessagesContent += '<div class="btn-group btn-group-small pull-right">';
    divIndMessagesContent += '<input type="hidden" name="hdnCommonMessageThreadId" id="hdnCommonMessageThreadId">';   
    if(messageJSON.messageStatus === 'Saved'){
        divIndMessagesContent += '<button class="btn btn-primary btn-small" title="Send this message" data-toggle="tooltip" onClick="invokeController(\'sendFromDrafts\');">';
        divIndMessagesContent += '<i class = "fa fa-arrow-circle-right fa-small"> Send </i></button>';
    }
    else{
        divIndMessagesContent += '<button class="btn btn-primary btn-small" title="Reply to this message" data-toggle="tooltip" onClick="loadReply();">';
        divIndMessagesContent += '<i class = "fa fa-reply fa-small"> Reply </i></button>';
    }    
    divIndMessagesContent += '<button class="btn btn-primary btn-small dropdown-toggle" title="More Options" data-toggle="dropdown">';
    divIndMessagesContent += '<i class = "fa fa-angle-down fa-small"></i></button>';
    divIndMessagesContent += '<ul class="dropdown-menu pull-right">';
    if(messageJSON.messageStatus === 'Saved'){
        divIndMessagesContent += '<li><a href="#" onClick="invokeController(\'sendFromDrafts\');"><i class ="fa fa-arrow-circle-right"> Send </i></a></li>';
    }
    else{
        divIndMessagesContent += '<li><a href="#" onClick="loadReply();"><i class ="fa fa-reply"> Reply </i></a></li>';   
    }
    //divIndMessagesContent += '<li><a href="#" onClick="invokeController(\'spam\');"><i class ="fa fa-ban"> Report spam </i></a></li>';
    divIndMessagesContent += '<li><a href="#" onClick="invokeController(\'delete\');"><i class ="fa fa-trash-o"> Delete message </i></a></li>';
    divIndMessagesContent += '</div>';
    divIndMessagesContent += '<div class="spacer5 pull-right">';
    divIndMessagesContent += '<button class="btn btn-primary btn-small pull-right" title="Delete this message" data-toggle="tooltip" onClick="invokeController(\'delete\');">';
    divIndMessagesContent += '<i class = "fa fa-trash-o"></i></button>';
    divIndMessagesContent += '</div>';
    divIndMessagesContent += '</div>';
    $("#divIndMessages").html(divIndMessagesContent);
    $("#hdnMessageThreadId").val(messageJSON._id);
    $("#hdnCommonMessageThreadId").val(messageJSON.commonMessageThreadId);
    //$("#messageSenderDetails").html(messageJSON.senderUsername + " on " + dateSent.toUTCString());
    $("#messageSubject").html(messageJSON.subject);    
    //alert($("#messageSubject").html());
    $("#tblDisplayMessages").hide();
    $("#divDisplayMessage").show();
}

//Function to close the display message div
function displayAllMessages(){
    $("#tblDisplayMessages").show();
    $("#divDisplayMessage").hide();   
    $("#divReply").hide();
    $("#refreshInbox").show();
    $("#paginationIcons").show();
}

//Function to load the image chosen to be cropped and previewed
function loadPreview(input){    
    if(input.files && input.files[0]){
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        reader.onload = function(e){                      
            $('#thumbnail').remove();            
            $('#thumbnail').attr('src', e.target.result);                
            $('#thumbnail').Jcrop({
                setSelect:   [ 100, 100, 50, 50 ],
                aspectRatio: 1 / 1,
                onRelease: showCoords                
            });            
        }        
    }
}

//function to set the image coordinates
function showCoords(c){
    $('#x1') = c.x1;
    $('#y1') = c.y1;
    $('#x2') = c.x2;
    $('#y2') = c.y2;
    $('#w') = c.w;
    $('#h') = c.h;
}

//function to crop the selected area
function cropImage(event){
    event.preventDefault();
    //alert("am inside the cropImage function");
    var srcPath = $("#thumbnail").attr('src');        
    $("#newImagePath").val(srcPath);
    //alert($("#newImagePath").val());
    //alert(userId);
    //var ajaxURL = "/user/profile/photoCrop/" + userId;    
    $.ajax({
        type : 'POST', 
        url : '/user/photoCrop',
        data : {srcPath : "sample data"}        
    }).done(function(response){
        if(response.msg === ''){  
            alert("call successful");          
        }
        else{
            alert("An error occurred while cropping the image: " + response.msg);
        }
    });   
    alert("after ajax call"); 
}


//Function to display the personal details section to display initial undefined values as blanks.
function loadPersonalDetailsSection(){    
    //alert($('#hdnGender').val());  
    if($('#firstname').val() == 'undefined'){
        $('#firstname').val("");
    }
    if($('#lastname').val() === 'undefined'){           
        $('#lastname').val("");
    }
    if($('#age').val() == 'null'){        
        $('#age').val("");        
    }
    if($('#age').val() == 'undefined'){        
        $('#age').val("");        
    }
    if($('#occupation').val() == 'undefined'){
        $('#occupation').val("");
    }
    if($('#locality').val() == 'undefined'){
        $('#locality').val("");
    }
    if($('#city').val() == 'undefined'){
        $('#city').val("");
    }
    if($('#country').val() == 'undefined'){
        $('#country').val("");
    }
    $('#gender').val($('#hdnGender').val());
    //alert($('#gender').val());
}

//Function to toggle the collapsible panel on a page.
function togglePanel(){
    var $this = $(this);
    if(!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    }
}

//Function that calculates the rating of an entered/edited experience
function calculateRating(){
    //alert("am inside the calculate rating method");
    //alert(liftServiceJSON[$('#lift').val()].ratingValue);
    //alert(ratingWeights.lift);
    //alert("am in!");
    var liftWeightScore = (parseInt(liftServiceJSON[$('#lift').val()].ratingValue) * parseInt(ratingWeights.lift))/100;
    var carParkScore = (parseInt(carParkJSON[$('#carpark').val()].ratingValue) * parseInt(ratingWeights.carpark))/100;
    var storesNearbyScore = (parseInt(storesNearbyJSON[$('#stores').val()].ratingValue) * parseInt(ratingWeights.stores))/100;
    var entertainmentVenuesScore = (parseInt(entertainmentVenuesJSON[$('#entertain').val()].ratingValue) * parseInt(ratingWeights.entertain))/100;
    var transportConnectivityScore = (parseInt(transportConnectivityJSON[$('#transport').val()].ratingValue) * parseInt(ratingWeights.transport))/100;
    //alert($('#watersupp').val());
    var waterSupplyScore = (parseInt(waterSupplyJSON[$('#watersupp').val()].ratingValue) * parseInt(ratingWeights.watersupp))/100;
    var powerSupplyScore = (parseInt(powerSupplyJSON[$('#powersupp').val()].ratingValue) * parseInt(ratingWeights.powersupp))/100;
    var drainPipesQualityScore = (parseInt(drainPipesQualityJSON[$('#drainqual').val()].ratingValue) * parseInt(ratingWeights.drainqual))/100;
    var electricPlugsSwitchesConditionScore = (parseInt(electricPlugsSwitchesConditionJSON[$('#eleccond').val()].ratingValue) * parseInt(ratingWeights.eleccond))/100;
    var withinCompoundAmenitiesScore = 0;        
    $('input[name="incompamen"]:checked').each(function() {       
       withinCompoundAmenitiesScore = withinCompoundAmenitiesScore + parseInt(withinCompoundAmenitiesJSON[$(this).val()].ratingValue);       
    });
    withinCompoundAmenitiesScore = (withinCompoundAmenitiesScore * parseInt(ratingWeights.incompamen))/100;    
    var trafficCongestionScore = (parseInt(trafficCongestionJSON[$('#traffic').val()].ratingValue) * parseInt(ratingWeights.traffic))/100;
    var defectsDetectedScore = (parseInt(defectsDetectedJSON[$('#defects').val()].ratingValue) * parseInt(ratingWeights.defects))/100;
    var totalRating = (liftWeightScore + carParkScore + storesNearbyScore + entertainmentVenuesScore + transportConnectivityScore + waterSupplyScore
                        + powerSupplyScore + drainPipesQualityScore + electricPlugsSwitchesConditionScore + withinCompoundAmenitiesScore
                        + trafficCongestionScore + defectsDetectedScore);    
    //alert(totalRating);
    $("#rating").val(totalRating);    
}

//Function to display the property type text in the Search Experience Results page.
function displayPropertyTypeText(){
    $('#tdType').html(propTypeJSON[$('#hdntype').val()]);
}

//Function to retrieve the text values of certain fields from globalJSON.js and display it on the page.
function displayExpValueText(){
    //alert("am in the displayExpValueText function");
    //alert($('#hdnLiftService').val());          
    //alert(liftServiceJSON[$('#hdnLiftService').val()].ratingValue);
    $('#tdType').html(propTypeJSON[$('#hdntype').val()]);
    $('#tdLiftService').html(liftServiceJSON[$('#hdnLiftService').val()].dispText);
    $('#tdPropertyCondition').html(propConditionJSON[$('#hdnPropertyCondition').val()]);
    $('#tdCarPark').html(carParkJSON[$('#hdnCarPark').val()].dispText);
    $('#tdStoresNearby').html(storesNearbyJSON[$('#hdnStoresNearby').val()].dispText);
    $('#tdEntertainmentVenues').html(entertainmentVenuesJSON[$('#hdnEntertainmentVenues').val()].dispText);
    $('#tdTransportConnectivity').html(transportConnectivityJSON[$('#hdnTransportConnectivity').val()].dispText);
    $('#tdWaterSupply').html(waterSupplyJSON[$('#hdnWaterSupply').val()].dispText);
    $('#tdPowerSupply').html(powerSupplyJSON[$('#hdnPowerSupply').val()].dispText);
    $('#tdDrainPipesQuality').html(drainPipesQualityJSON[$('#hdnDrainPipesQuality').val()].dispText);
    $('#tdElectricPlugsSwitchesCondition').html(electricPlugsSwitchesConditionJSON[$('#hdnElectricPlugsSwitchesCondition').val()].dispText);
    /*for(value in $('#hdnWithinCompoundAmenities').val().split(',')){
        $('#tdWithinCompoundAmenities').html($('#tdWithinCompoundAmenities').html() + ',');
    }*/
    var withinCompoundAmenities = "";
    if ($('#hdnWithinCompoundAmenities').val().indexOf("1") >  -1){      
        withinCompoundAmenities = withinCompoundAmenities + withinCompoundAmenitiesJSON[1].dispText;      
    }
    if ($('#hdnWithinCompoundAmenities').val().indexOf("2") >  -1){  
      if (withinCompoundAmenities != "")
        withinCompoundAmenities = withinCompoundAmenities + ',' + withinCompoundAmenitiesJSON[2].dispText;
      else
        withinCompoundAmenities = withinCompoundAmenities + withinCompoundAmenitiesJSON[2].dispText;
    }   
    if ($('#hdnWithinCompoundAmenities').val().indexOf("3") >  -1){  
      if (withinCompoundAmenities != "")
        withinCompoundAmenities = withinCompoundAmenities + ',' + withinCompoundAmenitiesJSON[3].dispText;
      else
        withinCompoundAmenities = withinCompoundAmenities + withinCompoundAmenitiesJSON[3].dispText;
    }
    if ($('#hdnWithinCompoundAmenities').val().indexOf("4") >  -1){
      if (withinCompoundAmenities != "")
        withinCompoundAmenities = withinCompoundAmenities + ',' + withinCompoundAmenitiesJSON[4].dispText;
      else
        withinCompoundAmenities = withinCompoundAmenities + withinCompoundAmenitiesJSON[4].dispText;
    }
    $('#tdWithinCompoundAmenities').html(withinCompoundAmenities);
    $('#tdTrafficCongestion').html(trafficCongestionJSON[$('#hdnTrafficCongestion').val()].dispText);
    $('#tdDefectsDetected').html(defectsDetectedJSON[$('#hdnDefectsDetected').val()].dispText);
}

//Select the dropdown values of edit experience page based on the values from the database.
function selectDropdownValues(){
    //alert("am inside the function selectDropdownValues");
    var expId = $('#experienceId').val();
    var ajaxURL = '/userExperience/' + expId;
    //alert(ajaxURL);
    $.getJSON(ajaxURL,{ajax : 'yes'}, function(data, status){        
        var amenities = data.withinCompoundAmenities.split(',');
        //alert(amenities[3]);
        $('#type').val(data.type);
        $('#lift').val(data.liftService);
        $('#propcond').val(data.propertyCondition);
        $('#carpark').val(data.carPark);
        $('#stores').val(data.storesNearby);
        $('#entertain').val(data.entertainmentVenues);
        $('#transport').val(data.transportConnectivity);
        $('[name=watersupp]').val(data.waterSupply);
        $('[name=powersupp]').val(data.powerSupply);
        $('[name=drainqual]').val(data.drainPipesQuality);
        $('[name=eleccond]').val(data.electricPlugsSwitchesCondition);                
        //$("#incompamendiv").find('[value=' + data.withinCompoundAmenities.join('], [value=') + ']').prop("checked", true);
        //for(var i = 0; i < data.withinCompoundAmenities.length; i++){
            //$('#incompamendiv [value=' + amenities[i] + ']').attr('checked', 'checked');
        //}
        $('#incompamendiv [value="'+amenities.join('"],[value="')+'"]').prop('checked',true);
        $('[name=traffic]').val(data.trafficCongestion);
        $('[name=defects]').val(data.defectsDetected);        
    });
}

//Show login dialog
function showLoginDialogue(){
    //alert("am inside the show dialog function");
    $("#loginModalDialog").modal('show');
}

//Function that invokes an AJAX call to verify user and login
function loginAjaxCall(){       
    //if(strategy === 'local'){ 
        if($('#loginemail').val() === ''){
            alert('Please enter your email id!');
        }
        else if($('#loginpassword').val() === ''){
            alert('Please enter your password!');
        }
        else if(!(IsEmail($('#loginemail').val()))){
            alert('Please enter a valid email id!');
        }
        else{
            $.ajax({
                type: 'POST',
                url: '/login',
                data:  {email: $('#loginemail').val(), password: $('#loginpassword').val()}
            }).done(function(response){
                if(response.msg === ''){
                    //location.reload();
                    //
                    //alert(response.redirectPage);
                    window.location.replace(response.redirectPage);
                }
                else{
                    //alert('an error occurred while logging in');
                    //alert(response.msg);
                    $('#divLoginErrorMessage').html(response.msg);                
                }
            });
        }
    //}
    /*else{
        //alert("am not a local authentication call");
        //alert(strategy);
        $.ajax({
                type: 'GET',
                url: strategy,
                dataType: 'jsonp',
                jsonp: 'callback',
                jsonpCallback: 'jsonpCallback'         
            });
        function jsonpCallback(response){
            alert("am in the callback response");
            if(response.msg === ''){
                //location.reload();
                //
                //alert(response.redirectPage);
                window.location.replace(response.redirectPage);
            }
            else{
                //alert('an error occurred while logging in');
                //alert(response.msg);
                $('#divLoginErrorMessage').html(response.msg);                
            }
        }
    }*/
}

//Function that invokes an AJAX call to register a new user.
function signUpAjaxCall(){        
    if($('#signupemail').val() === ''){
        alert('Please enter your email id!');
    }
    else if(!(IsEmail($('#signupemail').val()))){
        alert('Please enter a valid email id!');
    }
    else if($('#signuppassword').val() === ''){
        alert('Please enter your password!');
    }
    else if($('#signupusername').val() === ''){
        alert('Please enter your username!');
    }
    
    //else if(!(IsEmail($('#signupemail').val()))){
        //alert('Please enter a valid email id!');
    //}
    else{
        $.ajax({
            type: 'POST',
            url: '/signup',
            data:  {email: $('#signupemail').val(), password: $('#signuppassword').val(), username: $('#signupusername').val()}
        }).done(function(response){
            if(response.msg === ''){
                //location.reload();
                //
                //alert(response.redirectPage);
                window.location.replace(response.redirectPage);
            }
            else{
                //alert('an error occurred while logging in');
                //alert(response.msg);
                $('#divSignupErrorMessage').html(response.msg);                
            }
        });
    }
}

//Function for email validation.
function IsEmail(email) {  
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

// Delete User
function deleteExperience(event) {    
    event.preventDefault();

    // Pop up a confirmation dialog
    var confirmation = confirm('Are you sure you want to delete this experience?');

    // Check and make sure the user confirmed
    if (confirmation === true) {

        // If they did, do our delete
        $.ajax({
            type: 'DELETE',
            url: '/userExperience/' + $(this).attr('rel')
        }).done(function( response ) {

            // Check for a successful (blank) response            
            if (response.msg === '') {
            }
            else {
                alert('Error while deleting: ' + response.msg);
            }

            // Update the table
            populateTable();

        });

    }
    else {

        // If they said no to the confirm, do nothing
        return false;

    }

};

// Fill table with data
function populateTable() {

    // Empty content string
    var tableContent = '';    
    // jQuery AJAX call for JSON
    $.getJSON( '/userExperience/myExperiences',{ajax : 'yes'}, function( data , status ) {         
        // For each item in our JSON, add a table row and cells to the content string
        $.each(data, function(){            
            tableContent += '<tr>';
            tableContent += '<td>' + this.locality + '</td>';
            tableContent += '<td>' + this.city + '</td>';
            tableContent += '<td>' + this.type + '</td>';
            tableContent += '<td>' + this.floor + '</td>';
            tableContent += '<td>' + this.rating + '</td>';
            tableContent += '<td><a href="#" class="deleteExperienceLink" rel="' + this._id + '">delete</a></td>';
            tableContent += '</tr>';
        });        
        // Inject the whole content string into our existing HTML table        
        $('.experienceBody').html(tableContent);
    });
};



