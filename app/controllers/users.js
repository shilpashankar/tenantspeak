/**
 * Module dependencies
 */

var mongoose = require('mongoose');
var User = mongoose.model('user');
var nodemailer = require('nodemailer');
var util = require("util"); 
var fs = require("fs"); 
var utils = require('../../lib/utils');
var im = require('imagemagick');

//Load all Users 

exports.load = function(req, res){
	var db = req.db;
	var users = db.model('user').find({}, {}, function(err,doc){
		res.send(doc);
	});
}

//Load login page
exports.loadLogin = function(req, res){
	var redirect_to = req.session.redirect_to ? req.session.redirect_to : '/';
	//console.log("load login : " + redirect_to);
  	//delete req.session.redirect_to;  
	if (req.isAuthenticated()){
		return res.redirect(redirect_to);
  }
  //req.flash('errormessage', message);
  /*res.render('users/login_modal', {
    title: 'Login',
    message: req.flash('loginmessage')
  });  */
	//req.session.redirect_to = req.originalUrl;
	req.session.redirect_to = req.session.redirect_to ? req.session.redirect_to : '/';
  	console.log("not logged in");
  	console.log(req.session.redirect_to);
    //req.session.loadLogin = "true";
    //console.log('inside the failure redirect method again : ' + req.flash('loginMessage'));
    var response = req.flash('loginMessage');
    console.log(response);
    //console.log("the session variable" + req.session.loginError);
    //req.flash('errormessage', loginMessage);
    return res.send({msg: response});   
}

//login
exports.login = function(req, res){
  console.log("am in login after authentication");  
  var redirect_to = req.session.redirect_to ? req.session.redirect_to : '/';
  delete req.session.redirect_to;  
  if(req.session.loadLogin === 'true'){
  	delete req.session.loadLogin;
  }
  //console.log("post login : " + redirect_to);	
  if (req.isAuthenticated()){
  		console.log(redirect_to);
		//return res.redirect(redirect_to);
		//console.log(req.originalUrl);
		//if it is a local login request, redirect it to the ajax function
		if(req.originalUrl === '/login'){
			return res.send({msg: '', redirectPage : redirect_to }); 
		}
		//else, do a regular redirection, if it is an oauth facebook or google login request.
		else{
			return res.redirect(redirect_to);
		}
  }
  //console.log("message : " + message);
  //req.flash('errormessage', message);
  
  //res.render('users/login', {
    //title: 'Login',
    //message: req.flash('error')
  //});  
}

//Load signup page
exports.loadSignup = function(req, res){
	//res.render('users/signup', {
    //title: 'Sign Up'});
	req.session.redirect_to = req.session.redirect_to ? req.session.redirect_to : '/';
	var response = req.flash('signupMessage');
	return res.send({msg: response});
}

//Signup
exports.signup = function(req, res){
	/*var user = new User();

  	user.local.username = req.body.username;
  	user.local.email = req.body.email;
  	user.local.password = req.body.password;

  	user.save(function (err) {
    if (!err) {
    	return req.logIn(user, function () {
        req.flash('success', 'Welcome ' + user.name + '. You just signed up!');
        res.redirect('/');
      });
    }
    req.flash('errormessage', err);
    res.render('users/signup', {
      user: user,
      message: req.flash('errormessage')
    });
  });*/
  console.log("am in signup after registration");  
  var redirect_to = req.session.redirect_to ? req.session.redirect_to : '/';
  if(req.session.loadLogin === 'true'){
  	delete req.session.loadLogin;
  }
  /*delete req.session.redirect_to;  
  if(req.session.loadLogin === 'true'){
  	delete req.session.loadLogin;
  }*/
  //console.log("post login : " + redirect_to);	
  if (req.isAuthenticated()){
  		console.log(redirect_to);
		//return res.redirect(redirect_to);
		return res.send({msg: '', redirectPage : redirect_to }); 
  }
}

//Logout
exports.logout = function(req, res){
	req.logout();
	res.redirect('/');
}

//Email user function

exports.emailTenant = function(req, res){
	// create reusable transporter object using SMTP transport
	var transporter = nodemailer.createTransport({
	    service: 'Gmail',
	    auth: {
	        user: 'shilp.shankar@gmail.com',
	        pass: 'shil0804'
	    }
	});

	//Obtain and set the logged in user's email ID and the recipient's email id.
	var recipientEmail = req.headers['recipientemail'];
	var emailSubject = req.headers['emailsubject'];
	var emailContent = req.headers['emailcontent'];
	var senderEmail = 'shilp.shankar@gmail.com' //change this later to retrieve the email id of the logged in user from the session.

	// NB! No need to recreate the transporter object. You can use
	// the same transporter object for all e-mails

	// setup e-mail data with unicode symbols
	var mailOptions = {
	    from: senderEmail, // sender address
	    to: recipientEmail, // list of receivers
	    subject: 'ResidentExperiences : ' + emailSubject, // Subject line
	    text: emailContent, // plaintext body	   
	};

	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        console.log(error);
	    }else{
	        console.log('Message sent: ' + info.response);
	        res.send(info);
	    }
	});
}

//Load Edit Profile page
exports.loadEditProfile = function(req, res){
	console.log('am in display the edit profile page method');
	var db = req.db;		
	var userDetails = db.model('user').findOne({_id : req.params.currentUserId}, function(err,doc){
		if(err){
			console.log(err);
			res.send("A problem occurred while retrieving your data");
		}
		else{
			console.log(doc);			
			res.render('users/editProfile',{result:doc});				
		}
	});	
}

//Save edit profile changes
exports.editProfile = function(req, res){
	console.log("am inside the post method of edit profile");
	var db = req.db;
	var user = db.model('user');
	//console.log(req.body);
	//Find out a more elegant way to traverse through the req.body JSON to replace the undefined values by a blank or 0.
	if(req.body.firstname == 'undefined'){
		req.body.firstname = "";
	}
	if(req.body.lastname == 'undefined'){
		req.body.lastname = "";
	}
	if(req.body.age == ""){
		req.body.age = null;
	}
	if(req.body.occupation == 'undefined'){
		req.body.occupation = "";
	}
	if(req.body.locality == 'undefined'){
		req.body.locality = "";
	}
	if(req.body.city == 'undefined'){
		req.body.city = "";
	}
	if(req.body.country == 'undefined'){
		req.body.country = "";
	}	
	var modifiedUser = {
		"firstname" : req.body.firstname,
		"lastname" : req.body.lastname,
		"age" : req.body.age,
		"gender" : req.body.gender,
		"occupation" : req.body.occupation,
		"locality" : req.body.locality,
		"city" : req.body.city,
		"country" : req.body.country
	}
	if(req.user.twitter.username != null){
		modifiedUser.twitter = {
			id           : req.user.twitter.id,
	        token        : req.user.twitter.token,
	        displayName  : req.user.twitter.displayname,
	        username     : req.user.twitter.username
			}
	}
	else if(req.user.facebook.username != null){
		modifiedUser.facebook = {
			id           : req.user.facebook.id,
	        token        : req.user.facebook.token,
	        email        : req.user.facebook.email,
	        username     : req.user.facebook.username
			}
	}
	else if(req.user.google.username != null){
		modifiedUser.google = {
			id           : req.user.google.id,
	        token        : req.user.google.token,
	        email        : req.user.google.email,
	        username     : req.user.google.username
			}
	}
	else{
		modifiedUser.local = {				        
	        email           : req.user.local.email,
	        username        : req.user.local.username,
	        password        : req.user.local.password
		}
	}
	console.log(modifiedUser);
	user.findByIdAndUpdate(req.params.currentUserId, modifiedUser, function(err, doc){
		if(err){
			console.log(err);
			res.send("An error occured while trying to fetch the requested data");
		}
		else{
			//res.send(doc);
			res.redirect("/");
		}
	});
}

exports.loadUploadPhoto = function(req, res){	
	res.render('users/uploadPhoto');
}

exports.uploadPhoto = function(req, res){
	if (req.files) { 
		//console.log(util.inspect(req.files));
		console.log(req.files);
		if (req.files.imageUpload.size === 0) {
		            return next(new Error("Hey, first would you select a file?"));
		}
		fs.exists(req.files.imageUpload.path, function(exists) { 
			if(exists) { 
				var oldPath = req.files.imageUpload.path;
				var newPath = "public\\uploads\\img_"+ req.user._id + '.jpg';
				console.log('Old Path is: ' + oldPath);
				console.log('New Path is: ' + newPath);
				fs.rename(oldPath, newPath, function(err){
					if(err){
						console.log("An error occurred while renaming your file" + err);
						res.send("An error occurred while uploading your picture");
					}
					else{
						console.log("File rename successful");
						res.send("File rename successful");
					}
				});				
				//res.send("Got your file!"); 
			} else { 
				res.send("Error!! The selected file could not be found.");
			} 
		}); 
	} 
}

exports.cropPhoto = function(req, res){
	console.log("am inside the cropPhoto method");
	//console.log(srcPath);
	console.log("user id is : " + req.user._id);
	console.log("image path is : " + req.query);
}
