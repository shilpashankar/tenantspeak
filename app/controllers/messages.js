/**
 * Module dependencies
 */

var mongoose = require('mongoose');
var MessageThread = mongoose.model('messageThread');
var nodemailer = require('nodemailer');
var util = require("util"); 
var moment = require("moment");

//console.log("message thread is : " + MessageThread);
exports.showInbox = function(req, res){
	console.log("am inside the showInbox method");
	var db = req.db;
	console.log(req.user._id);	
	db.model('messageThread').paginate({$or : 
	[
		{
		$and : 
		[
			{"userid": req.user._id},
			{"messageStatus" : "Received"}
		]
		},
		{
			$and : 
			[
				{"userid": req.user._id},
				{"messageStatus" : "Sent"},
				{"hasReply" : true}
			]
		}
	]}, req.query.page, req.query.limit, function(err, pageCount, userMessages, itemCount){
		if(err){
			console.log(err);
			res.send("An error occurred while processing this request!!");
		}
		else{
			console.log(userMessages);			
			//var userMessagesJSON = JSON.parse(userMessages);
			//console.log("modified JSON : " + userMessagesJSON);
			//res.render('messageThreads/displayMessages', {inboxMessages: userMessages, moment: moment});
			//console.log("req.query.page is: " + req.query.page);
			//console.log("req.query.limit is: " + req.query.limit);
			//console.log("Page count is: " + pageCount);
			//console.log("Item count is: " + itemCount);
			res.format({
		      html: function() {
		        res.render('messageThreads/displayMessages', {
		          inboxMessages: userMessages,
		          pageCount: pageCount,
		          itemCount: itemCount,
		          moment: moment
		        });
		      },
		      json: function() {
		        // inspired by Stripe's API response for list objects
		        res.json({
		          object: 'list',
		          has_more: paginate.hasNextPages(req)(pageCount),
		          data: inboxMessages
		        });
		      }
		    });
		}
	},{sortBy: {dateModified : -1}});	
}

exports.showSentItems = function(req, res){
	console.log("am inside the showSentItems method");
	var db = req.db;
	console.log(req.user._id);
	db.model('messageThread').paginate({
		$and : 
		[
			{userid: req.user._id},
			{"messageStatus" : "Sent"}
		]
	},req.query.page, req.query.limit, function(err, pageCount, userMessages, itemCount){
		if(err){
			console.log(err);
			res.send("An error occurred while processing this request!!");
		}
		else{
			console.log(userMessages);		
			//res.render('messageThreads/displayMessages', {inboxMessages: userMessages, moment: moment});
			res.format({
		      html: function() {
		        res.render('messageThreads/displayMessages', {
		          inboxMessages: userMessages,
		          pageCount: pageCount,
		          itemCount: itemCount,
		          moment: moment
		        });
		      },
		      json: function() {
		        // inspired by Stripe's API response for list objects
		        res.json({
		          object: 'list',
		          has_more: paginate.hasNextPages(req)(pageCount),
		          data: inboxMessages
		        });
		      }
		    });
		}
	},{sortBy: {dateModified : -1}});	
}

exports.showDrafts = function(req, res){
	console.log("am inside the showDrafts method");
	var db = req.db;
	console.log(req.user._id);
	db.model('messageThread').paginate({
		$and : 
		[
			{userid: req.user._id},
			{"messageStatus" : "Saved"}
		]
	},req.query.page, req.query.limit, function(err, pageCount, userMessages, itemCount){
		if(err){
			console.log(err);
			res.send("An error occurred while processing this request!!");
		}
		else{			
			console.log(userMessages);			
			//res.render('messageThreads/displayMessages', {inboxMessages: userMessages, moment: moment});
			res.format({
		      html: function() {
		        res.render('messageThreads/displayMessages', {
		          inboxMessages: userMessages,
		          pageCount: pageCount,
		          itemCount: itemCount,
		          moment: moment
		        });
		      },
		      json: function() {
		        // inspired by Stripe's API response for list objects
		        res.json({
		          object: 'list',
		          has_more: paginate.hasNextPages(req)(pageCount),
		          data: inboxMessages
		        });
		      }
		    });
		}
	},{sortBy: {dateModified : -1}});	
}

//TODO: Insert a record into the common message thread collection and include the id in the sender copy.
exports.saveDraft = function(req, res){
	var db = req.db;
	var senderEmail;	
	var senderUsername;	
	if(req.user.local.username !== undefined){
		senderUsername = req.user.local.username;
		senderEmail = req.user.local.email;
	}
	else if(req.user.google.username !== undefined){		
		senderUsername = req.user.google.username;
		senderEmail = req.user.google.email;
	}
	else if(req.user.facebook.username !== undefined){
		senderUsername = req.user.facebook.username;
		senderEmail = req.user.facebook.email;
	}
	else if(req.user.twitter.username !== undefined){
		senderUsername = req.user.twitter.username;
		senderEmail = req.user.twitter.email;
	}	
	
	//Inserting a record into the commonMessageThreadCollec collection.
	db.model('commonMessageThread').create({}, function(err, data){
		if(err){
			console.log(err);
			res.send("An error occured while saving the message : " + err);
		}
		else{ 

			console.log(data);
			//res.redirect("/messages");	
			var senderMessage = {
				"subject" : req.body.inputSubject,
				"isSpam" : false,
				"sender": senderEmail,
				"recipient": req.body.recipientEmail,
				"senderUsername": senderUsername,
				"recipientUsername": req.body.hdnRecipientUsername,
				"messages": [{			
					"senderUsername": senderUsername,
					"content": req.body.inputContent,
					"readStatus": "Unread"								
				}],
				"messageStatus": "Saved",
				"hasReply": false,		
				"userid": mongoose.Types.ObjectId(req.user._id),
				"commonMessageThreadId": mongoose.Types.ObjectId(data._id)
			}

			//Inserting into the sender sent
			db.model('messageThread').create(senderMessage, function(err, senderMessage){
				if(err){
					console.log(err);
					res.send("An error occured while sending the message : " + err);
				}
				else{ 
					console.log(senderMessage);
					res.redirect("/");				
				}
			});
		}
	});
}


exports.loadComposeMessage = function(req, res){
	//console.log("am inside the loadComposeMessage method " + req.params.recipientId);
	var db = req.db;
	var recipientUsername;
	var recipientEmail;
	var recipientUser = db.model('user').findOne({_id: req.params.recipientId}, function(err, doc){
		if(err){
			console.log(err);
			res.send("There was a problem in retrieving the recipient details!! " + err);
		}
		else{			
			if(doc.local.username !== undefined){
				recipientUsername = doc.local.username;
				recipientEmail = doc.local.email;
			}
			else if(doc.google.username !== undefined){
				console.log("am a google user");
				recipientUsername = doc.google.username;
				recipientEmail = doc.google.email;
			}
			else if(doc.facebook.username !== undefined){
				recipientUsername = doc.facebook.username;
				recipientEmail = doc.facebook.email;
			}
			else if(doc.twitter.username !== undefined){
				recipientUsername = doc.twitter.username;
				recipientEmail = doc.twitter.email;
			}			
			res.render('messageThreads/composeMessage', {recipientId : doc._id, recipientUsername : recipientUsername, recipientEmail : recipientEmail });
		}
	});	
}

//TODO: Insert a record into the common message thread collection and include the id in the sender and recipient copy.
exports.composeMessage = function(req, res){
	var db = req.db;
	var senderEmail;	
	var senderUsername;
	
	if(req.user.local.username !== undefined){
		senderUsername = req.user.local.username;
		senderEmail = req.user.local.email;
	}
	else if(req.user.google.username !== undefined){		
		senderUsername = req.user.google.username;
		senderEmail = req.user.google.email;
	}
	else if(req.user.facebook.username !== undefined){
		senderUsername = req.user.facebook.username;
		senderEmail = req.user.facebook.email;
	}
	else if(req.user.twitter.username !== undefined){
		senderUsername = req.user.twitter.username;
		senderEmail = req.user.twitter.email;
	}
	//Inserting a record into the commonMessageThreadCollec collection.
	db.model('commonMessageThread').create({}, function(err, data){
		if(err){
			console.log(err);
			res.send("An error occured while saving the message : " + err);
		}
		else{ 
			console.log(data);
			var recipientMessage = {
				"subject" : req.body.inputSubject,
				"isSpam" : false,
				"sender": senderEmail,
				"recipient": req.body.recipientEmail,
				"senderUsername": senderUsername,
				"recipientUsername": req.body.hdnRecipientUsername,
				"messages": [{			
					"senderUsername": senderUsername,
					"content": req.body.inputContent,
					"readStatus": "Unread"							
				}],
				"messageStatus": "Received",
				"hasReply": false,
				"userid": mongoose.Types.ObjectId(req.body.recipientId),
				"commonMessageThreadId": mongoose.Types.ObjectId(data._id)
			}
			var senderMessage = {
				"subject" : req.body.inputSubject,
				"isSpam" : false,
				"sender": senderEmail,
				"recipient": req.body.recipientEmail,
				"senderUsername": senderUsername,
				"recipientUsername": req.body.hdnRecipientUsername,
				"messages": [{
					"senderUsername": senderUsername,			
					"content": req.body.inputContent,
					"readStatus": "Unread"								
				}],
				"messageStatus": "Sent",
				"hasReply": false,
				"userid": mongoose.Types.ObjectId(req.user._id),
				"commonMessageThreadId": mongoose.Types.ObjectId(data._id)
			}
			//Inserting into the recipient inbox
			db.model('messageThread').create(recipientMessage, function(err, recipientMessage){
				if(err){
					console.log(err);
					res.send("An error occured while sending the message : " + err);
				}
				else{
					console.log(recipientMessage);					
				}
			});
			//Inserting into the sender sent
			db.model('messageThread').create(senderMessage, function(err, senderMessage){
				if(err){
					console.log(err);
					res.send("An error occured while sending the message : " + err);
				}
				else{ 
					console.log(senderMessage);
					//res.redirect("/");				
				}
			});
			// create reusable transporter object using SMTP transport
			var transporter = nodemailer.createTransport({
			    service: 'Gmail',
			    auth: {
			        user: 'shilp.shankar@gmail.com',
			        pass: 'adminshil0804'
			    }
			});

			//Obtain and set the logged in user's email ID and the recipient's email id.
			var recipientEmail = req.body.recipientEmail;
			var emailSubject = req.body.inputSubject;
			var emailContent = req.body.inputContent;
			//var senderUsername = senderUsername; //change this later to retrieve the email id of the logged in user from the session.

			// NB! No need to recreate the transporter object. You can use
			// the same transporter object for all e-mails

			// setup e-mail data with unicode symbols
			var mailOptions = {
			    from: senderUsername, // sender address
			    to: recipientEmail, // list of receivers
			    subject: 'ResidentExperiences : ' + emailSubject, // Subject line
			    text: emailContent, // plaintext body	   
			};

			// send mail with defined transport object
			transporter.sendMail(mailOptions, function(error, info){
			    if(error){
			        console.log(error);
			    }else{
			        console.log('Message sent: ' + info.response);
			        //res.send(info);
			    }
			});
		}
		res.redirect("/");
	});	
}

// TODO : Depending on the replied common message thread id, update the reply recipient's message copy also, with the reply and hasReply field.
exports.sendReply = function(req, res){
	//console.log("am inside the sendReply method");
	var senderUsername;
	var db = req.db;
	var replyRecipientUserId;
	//console.log(req.body.replyContent);
	//console.log(req.params.messageThreadId);
	if(req.user.local.username !== undefined){
		senderUsername = req.user.local.username;
		senderEmail = req.user.local.email;
	}
	else if(req.user.google.username !== undefined){		
		senderUsername = req.user.google.username;
		senderEmail = req.user.google.email;
	}
	else if(req.user.facebook.username !== undefined){
		senderUsername = req.user.facebook.username;
		senderEmail = req.user.facebook.email;
	}
	else if(req.user.twitter.username !== undefined){
		senderUsername = req.user.twitter.username;
		senderEmail = req.user.twitter.email;
	}

	replyMessage = {
		"senderUsername": senderUsername,
		"content": req.body.replyContent,
		"readStatus": "Unread",
		"dateSent": Date.now
	}
	//console.log(replyMessage);
	db.model('messageThread').update({"_id" : req.params.messageThreadId},{$push : {"messages": replyMessage}},function(err, data){
		if(err){
			console.log("An error occured while processing this request" + err);
			res.send({msg:'error: ' + err});
		}
		else{			
			db.model('messageThread').update({"_id" : req.params.messageThreadId},{"hasReply" : true},function(err, data){
				if(err){
					console.log("An error occured while processing this request" + err);
					res.send({msg:'error: ' + err});
				}
				else{
					console.log("successfully updated" + data);
					res.send({msg:''});
				}
			});
		}
	});	
	//Updating the recipient's message copy
	db.model('messageThread').update({
		$and : 
		[
			{"commonMessageThreadId": req.body.commonMessageThreadId},
			{"userid" : {$ne : mongoose.Types.ObjectId(req.user._id)}}
		]},{$push : {"messages": replyMessage}},function(err, data){
			if(err){
				console.log("An error occured while processing this request" + err);
				res.send({msg:'error: ' + err});
			}
			else{
				db.model('messageThread').update({
					$and :
					[
						{"commonMessageThreadId": req.body.commonMessageThreadId},
						{"userid" : {$ne : mongoose.Types.ObjectId(req.user._id)}}	
					]},{"hasReply" : true},function(err, data){
						if(err){
							console.log("An error occured while processing this request" + err);
							res.send({msg:'error: ' + err});
						}
						else{
							console.log("successfully updated" + data);
							//Retrieve the messagethread data needed to send an email notification.
							db.model('messageThread').findOne({_id : req.params.messageThreadId}, function(err, messageDetails){
								if(err){
									console.log("An error occured while processing this request" + err);
									res.send({msg:'error: ' + err});
								}
								else{
									// create reusable transporter object using SMTP transport
									var transporter = nodemailer.createTransport({
									    service: 'Gmail',
									    auth: {
									        user: 'shilp.shankar@gmail.com',
									        pass: 'adminshil0804'
									    }
									});

									//Obtain and set the logged in user's email ID and the recipient's email id.
									var recipientEmail = messageDetails.recipient;
									var emailSubject = messageDetails.subject;
									var emailContent = messageDetails.messages[0].content;
									//var senderUsername = senderUsername; //change this later to retrieve the email id of the logged in user from the session.

									// NB! No need to recreate the transporter object. You can use
									// the same transporter object for all e-mails

									// setup e-mail data with unicode symbols
									var mailOptions = {
									    from: messageDetails.senderUsername, // sender address
									    to: recipientEmail, // list of receivers
									    subject: 'ResidentExperiences : ' + emailSubject, // Subject line
									    text: emailContent, // plaintext body	   
									};

									// send mail with defined transport object
									transporter.sendMail(mailOptions, function(error, info){
									    if(error){
									        console.log(error);
									    }else{
									    	console.log("mail options were : " + mailOptions);
									        console.log('Message sent: ' + info.response);
									        //res.send(info);
									    }
									});
								}
							});
							res.send({msg:''});
						}
				});
			}
	});	
}

// TODO : Check if both the sender and recipient copies of the message thread has been deleted and if so delete the corresponding commonMessageThread document.
exports.deleteMessageThread = function(req, res){
	console.log("am inside the deleteMessageThread method " + req.params.messageThreadId);
	var db = req.db;
	db.model('messageThread').remove({"_id" : req.params.messageThreadId}, function(err, data){
		if(err){
			console.log("An error occured while processing this request" + err);
			res.send({msg:'error: ' + err});
		}
		else{
			console.log("successfully deleted" + data);
			//res.send({msg:''});	
			db.model('messageThread').find({				
					$and :[
						{"commonMessageThreadId": mongoose.Types.ObjectId(req.body.commonMessageThreadId)},
						{"userid" : {$ne : mongoose.Types.ObjectId(req.user._id)}}	
					]},function(err, data){
						//console.log(data);
						//console.log(req.body.commonMessageThreadId);
				if(err){
					console.log("An error occured while processing this request" + err);
					res.send({msg:'error: ' + err});
				}
				else{
					//console.log("successfully deleted" + data);
					//console.log(data);
					//console.log(req.body.commonMessageThreadId);
					//console.log("hello!!" + data[0].commonMessageThreadId.equals(req.body.commonMessageThreadId));
					if(data[0] !== undefined && data[0].commonMessageThreadId.equals(req.body.commonMessageThreadId)){
						//console.log(data.commonMessageThreadId);
						//console.log(req.body.commonMessageThreadId);
						res.send({msg:''});
					}				
					else{
						db.model('commonMessageThread').remove({"_id" : mongoose.Types.ObjectId(req.body.commonMessageThreadId)},function(err, data){
							if(err){
								console.log("An error occured while processing this request" + err);
								res.send({msg:'error: ' + err});
							}
							else{
								console.log("successfully deleted common message thread also" + data);
								res.send({msg:''});
							}
						});
					}
				}
			});
		}
	});
}

exports.updateMessageReadStatus = function(req, res){
	console.log("am inside the updateMessageReadStatus function");
	console.log(req.params.messageId);
	var db = req.db;
	db.model('messageThread').update({"messages._id" : req.params.messageId},{$set: {"messages.$.readStatus" : "Read"}}, function(err, data){
		if(err){
			console.log("An error occured while processing this request" + err);
			res.send({msg:'error: ' + err});
		}
		else{
			console.log("successfully updated the message status " + data);
			res.send({msg:''});
		}
	});
}

exports.sendMessageFromDrafts = function(req, res){
	var db = req.db;
	console.log("am inside the sendMessageFromDrafts function");
	console.log(req.params.messageThreadId);	
	db.model('messageThread').findOne({"_id" : req.params.messageThreadId}, function(err, data){
		if(err){
			console.log("An error occured while processing this request " + err);
			res.send({msg:'error: ' + err});
		}
		else{
			console.log("Retrieved message is : " + data);
			//TODO : write a query to fetch the recipient id from the recipient username
			console.log("message details retrieved: " + data.recipientUsername);
			var recipientUser = db.model('user').findOne({
				$or :[
						{
							"local.username" : data.recipientUsername						
						},
						{
							"google.username" : data.recipientUsername							
						},
						{
							"facebook.username" : data.recipientUsername					
						},
						{
							"twitter.username" : data.recipientUsername							
						}
					]
			}, function(err, doc){
				if(err){
					console.log("An error occured while processing this request : " + err);
					res.send({msg:'error: ' + err});
				}
				else{
					console.log("Recipient user id is : " + doc._id);
					var recipientMessage = {
						"subject" : data.subject,
						"isSpam" : data.isSpam,
						"sender": data.sender,
						"recipient": data.recipient,
						"senderUsername": data.senderUsername,
						"recipientUsername": data.recipientUsername,
						"messages": [{			
							"senderUsername": data.senderUsername,
							"content": data.messages[0].content,
							"readStatus": "Unread"								
						}],
						"messageStatus": "Received",
						"hasReply": data.hasReply,		
						"userid": doc._id,
						"commonMessageThreadId": mongoose.Types.ObjectId(data.commonMessageThreadId)
					}

					//Inserting into the sender sent
					db.model('messageThread').create(recipientMessage, function(err, recipientMessage){
						if(err){
							console.log(err);
							res.send({msg:'error: ' + err});
						}
						else{ 
							console.log("recipientMessage is : " + recipientMessage);					
							db.model('messageThread').update({"_id" : req.params.messageThreadId},{"messageStatus" : "Sent"}, function(err, data){
								if(err){
									console.log("An error occured while processing this request " + err);
									res.send({msg:'error: ' + err});
								}
								else{		
									console.log("Successfully updated the sender copy status and created the recipient copy of the message!");
									//Send the notification email.
									// create reusable transporter object using SMTP transport
									var transporter = nodemailer.createTransport({
									    service: 'Gmail',
									    auth: {
									        user: 'shilp.shankar@gmail.com',
									        pass: 'adminshil0804'
									    }
									});

									//Obtain and set the logged in user's email ID and the recipient's email id.
									var recipientEmail = recipientMessage.recipient;
									var emailSubject = recipientMessage.subject;
									var emailContent = recipientMessage.messages[0].content;
									//var senderUsername = senderUsername; //change this later to retrieve the email id of the logged in user from the session.

									// NB! No need to recreate the transporter object. You can use
									// the same transporter object for all e-mails

									// setup e-mail data with unicode symbols
									var mailOptions = {
									    from: recipientMessage.senderUsername, // sender address
									    to: recipientEmail, // list of receivers
									    subject: 'ResidentExperiences : ' + emailSubject, // Subject line
									    text: emailContent, // plaintext body	   
									};

									// send mail with defined transport object
									transporter.sendMail(mailOptions, function(error, info){
									    if(error){
									        console.log(error);
									    }else{
									    	console.log("mail options were : " + mailOptions);
									        console.log('Message sent: ' + info.response);
									        //res.send(info);
									    }
									});								
									res.send({msg:''});
								}
							});
						}
					});	
				}
			});			
		}
	});
}