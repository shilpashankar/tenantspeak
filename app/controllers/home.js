//Load the home page.
exports.index = function(req, res){
	res.render('home/new_home', { title: 'Express' });
}

//Load the privacy policy page
exports.privacypolicy = function(req, res){
	res.render('home/privacypolicy', {title: 'Privacy Policy'})
}

//Load the terms and conditions page
exports.termsandcondition = function(req, res){
	res.render('home/termsandconditions', {title: 'Terms and Conditions'})
}
