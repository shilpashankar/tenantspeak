/**
 * Module dependencies
 */

var mongoose = require('mongoose');
var User = mongoose.model('userExperience');

//Load the enter new experience page.
exports.loadSearch = function(req, res){	
	res.render('userExperiences/searchExperience', {
    title: 'Search for relevant experiences'   
  });
}

//Load all User Experiences based on the search criteria.
//Including pagination code.
exports.load = function(req, res){
	console.log('am in userExperience load function');
	var db = req.db;
	var getUserExpCriteria = {};	
	//For pagination.
	var perPage = 10;
	var page = req.param('page') > 0 ? req.param('page') : 0;
	console.log('page number is ' + page);
	if(req.query.locality !== ''){		
		getUserExpCriteria.locality = req.query.locality;		
	}
	if(req.query.city !== ''){		
		getUserExpCriteria.city = req.query.city;		
	}
	if(req.query.type !== '0'){		
		getUserExpCriteria.type = req.query.type;		
	}	
	console.log(getUserExpCriteria);
	db.model('userExperience').find(getUserExpCriteria,function(err, doc){
		if(err){
			console.log(err);
			res.send("A problem occurred while retrieving your data");
		}
		else{
			//db.model('userExperience').count().exec(function(err, count){
				//if(err){
					//console.log(err);
					//res.send("A problem occurred while retrieving your data");
				//}
				//else{
					res.render('userExperiences/searchExperienceResults', {title : 'Relevant Experiences', results : doc});
				//}
			//});
		}
	});
	/*db.model('userExperience').find(getUserExpCriteria, {}, function(err,doc){
		if(err){
			console.log(err);
			res.send("A problem occurred while retrieving your data");
		}
		else{
			console.log(doc);
			res.render(	'userExperiences/searchExperienceResults',{title : 'Relevant Experiences', results : doc});		
		}
	});*/
}

//Load the selected User Experience.
exports.loadById = function(req, res){
	console.log('am inside the loadById method');	
	var db = req.db;	
	var users = db.model('userExperience').findOne({_id : req.params.userExperienceId}, function(err,doc){
		if(err){
			console.log(err);
			res.send("A problem occurred while retrieving your data");
		}
		else{
			console.log(doc);	
			if(req.query.ajax !== undefined && req.query.ajax === 'yes'){
				res.send(doc);
			}
			else{
				res.render('userExperiences/displayUserExperience',{result:doc});
			}								
		}
	});
}

//Load the selected User Experience for editing.
exports.loadForEdit = function(req, res){
	console.log('am inside the loadForEdit method');
	var db = req.db;		
	var users = db.model('userExperience').findOne({_id : req.params.userExperienceId}, function(err,doc){
		if(err){
			console.log(err);
			res.send("A problem occurred while retrieving your data");
		}
		else{
			console.log(doc);			
			res.render('userExperiences/editUserExperience',{result:doc});				
		}
	});
}

//Load the experiences of the currently logged in user.
exports.showCurrUserExp = function(req, res){
	var db = req.db;	
	var ObjectId = mongoose.Types.ObjectId;
	console.log('am inside the showCurrUserExp method');
	//console.log(req.query.ajax);
	var users = db.model('userExperience').find({"userid" : req.user._id},{},{sort: {rating: -1}}, function(err,doc){
		if(err){
			console.log(err);
			res.send("A problem occurred while retrieving your data");
		}
		else
		{			
			console.log(doc);
			if(req.query.ajax !== undefined && req.query.ajax === 'yes'){
				res.send(doc);
			}
			else{
				res.render(	'userExperiences/myExperiences',{title : 'My Experiences', results : doc});	
			}	
		}					
	});
}

//Load the enter new experience page.
exports.new = function(req, res){
	console.log('am in display the new user exp router method');
	res.render('userExperiences/enterExperience', {
    title: 'New Experience'   
  });
}

//Create a new User Experience.
exports.create = function(req, res){	
	console.log('am in the POST method');
	var db = req.db;
	//console.log(req.headers);
	//console.log(req.body);
	console.log(req.body.incompamen);
	if(req.body.incompamen == undefined){
		req.body.incompamen = 0;
	}
	var inputExperience = {
		"locality" : req.body.locality,
		"city" : req.body.city,
		"type" : req.body.type,
		"floor" : req.body.floor,
		"liftService" : req.body.lift,
		"propertyCondition" : req.body.propcond,
		"carPark" : req.body.carpark,
		"storesNearby" : req.body.stores,
		"entertainmentVenues" : req.body.entertain,
		"transportConnectivity" : req.body.transport,
		"waterSupply" : req.body.watersupp,
		"powerSupply" : req.body.powersupp,
		"drainPipesQuality" : req.body.drainqual,
		"electricPlugsSwitchesCondition" : req.body.eleccond,
		"withinCompoundAmenities" : req.body.incompamen,
		"trafficCongestion" : req.body.traffic,
		"defectsDetected" : req.body.defects,
		"comments" : req.body.comments,
		"rating" : req.body.rating,
		"userid" : req.user._id
	};
	//console.log(inputExperience);
	var userExperience = db.model('userExperience');
	userExperience.create(inputExperience, function(err,inputExperience){
		if(err){
			console.log(err);
			res.send("A problem occurred while inserting your data");
		}
		else{
			console.log(inputExperience);
			res.redirect("/");
		}
	});
}

//Edit an existing User Experience.
exports.modify = function(req, res){
	var db = req.db;
	var userExperience = db.model('userExperience');
	console.log("am inside modify");
	//console.log(req.body);
	if(req.body.incompamen === undefined){
		req.body.incompamen = 0;
	}
	var modifiedExperience = {
		"locality" : req.body.locality,
		"city" : req.body.city,
		"type" : req.body.type,
		"floor" : req.body.floor,
		"liftService" : req.body.lift,
		"propertyCondition" : req.body.propcond,
		"carPark" : req.body.carpark,
		"storesNearby" : req.body.stores,
		"entertainmentVenues" : req.body.entertain,
		"transportConnectivity" : req.body.transport,
		"waterSupply" : req.body.watersupp,
		"powerSupply" : req.body.powersupp,
		"drainPipesQuality" : req.body.drainqual,
		"electricPlugsSwitchesCondition" : req.body.eleccond,
		"withinCompoundAmenities" : req.body.incompamen,
		"trafficCongestion" : req.body.traffic,
		"defectsDetected" : req.body.defects,
		"comments" : req.body.comments,
		"rating" : req.body.rating,
		"userid" : req.user._id
	};
	console.log(req.params.userExperienceId);
	userExperience.findByIdAndUpdate(req.params.userExperienceId, modifiedExperience, function(err, doc){
		if(err){
			console.log(err);
			res.send("An error occured while trying to fetch the requested data");
		}
		else{
			//res.send(doc);
			res.redirect("/");
		}
	});
}

//Delete a User Experience.
exports.remove = function(req, res){
	var db = req.db;
	var userExperience = db.model('userExperience');
	var expIdToBeDeleted = req.params.selectedUserExpId;
	console.log('am in the remove function');
	console.log(expIdToBeDeleted);
	userExperience.findByIdAndRemove(expIdToBeDeleted,function(err,doc){
		if(err){
			console.log(err); 
			res.send({msg:'error: ' + err});			
		}
		else{
			console.log('the deleted document is: ' + doc);
			res.send( {msg: ''});
		}
	});
}