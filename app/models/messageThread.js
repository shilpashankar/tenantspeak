var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');

var messages = new Schema({	
	senderUsername: {type: String, required: true},
	content: {type: String, required: false},
	readStatus: {type: String, required: true},
	dateSent: {type: Date, required: true, default: Date.now}
});
var messageThreadSchema = new Schema({
	sender: {type: String, required: true},
	recipient: {type: String, required: false},
	senderUsername: {type: String, required: true},
	recipientUsername: {type: String, required: true},
	subject : {type: String, required: false},
	isSpam : {type: String, required: true},
	messages: [messages],
	userid: {
		type : Schema.ObjectId,
		ref : 'user',
		required : true
	},
	hasReply: {type: Boolean, required: true},
	messageStatus: {type: String, required: true},
	dateModified: {type: Date, required: true, default: Date.now},
	commonMessageThreadId: {
		type : Schema.ObjectId,
		ref : 'commonMessageThreadId',
		required : true
	}
});

messageThreadSchema.plugin(mongoosePaginate);
mongoose.model('messageThread', messageThreadSchema, 'messagethreadcollec');
