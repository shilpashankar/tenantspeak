var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Create a tenant experience schema matching the tenant experience collection created in mongodb.
var userExperienceSchema = new Schema({
	locality : {type : String, required : true},
	city : {type : String, required : true},
	type : {type : String, required : true},
	floor : {type : String, required : false},
	liftService : {type : String, required : false},
	propertyCondition : {type : String, required : false},
	carPark : {type : String, required : false},
	storesNearby : {type : String, required : false},
	entertainmentVenues : {type : String, required : false},
	transportConnectivity : {type : String, required : false},
	waterSupply :{type : String, required : false},
	powerSupply : {type : String, required : false},
	drainPipesQuality : {type : String, required : false},
	electricPlugsSwitchesCondition : {type : String, required : false},
	withinCompoundAmenities : {type : String, required : false},
	trafficCongestion : {type : String, required : false},
	defectsDetected : {type : String, required : false},
	comments : {type : String, required : false},
	rating : {type : String, required : true},
	userid : {
		type : Schema.ObjectId,
		ref : 'user',
		required : true
	}
});

mongoose.model('userExperience', userExperienceSchema, 'userexperiencecollec');