var express = require('express');
var session    = require('express-session');
var mongoStore = require('connect-mongo')(session);
var pkg = require('../package');
var env = process.env.NODE_ENV || 'development';
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var flash = require('connect-flash');
var multer = require('multer');


module.exports = function (app, config, passport) {
	
	// view engine setup.	
	app.set('views', path.join(__dirname, '../app/views'));
	app.set('view engine', 'jade');	
	app.set('showStackError', true);	
	app.use(favicon());
	app.use(express.static(path.join(__dirname, '../public')));
	app.use(logger('dev'));
	app.use(bodyParser());
	app.use(cookieParser());
	app.use(methodOverride());
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(multer({dest: './public/uploads/'}));	
	//app.use(cors());

	app.use(session({
      secret: pkg.name,
      resave : false,
      saveUninitialized : false,
      store: new mongoStore({
        url: config.db,
        collection : 'sessions'        
      })
    }));

    if (passport) {
      app.use(passport.initialize());
      app.use(passport.session());
    }
	// Flash messages
    app.use(flash());    	
	// expose pkg to views
    app.use(function (req, res, next) {
      res.locals.pkg = pkg;
      next();
    });
    //Expose the request object to the view
    app.use(function (req, res, next) {
	    res.locals.req = req;
	    next();
	});
	//create pagination helper to be used in views.
	app.use(function (req, res, next){
		res.locals.createPagination = function (pages, page) {
			var url = require('url')
			  , qs = require('querystring')
			  , params = qs.parse(url.parse(req.url).query)
			  , str = ''

			params.page = 0
			var clas = page == 0 ? "active" : "no"
			str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">First</a></li>'
			for (var p = 1; p < pages; p++) {
			  params.page = p
			  clas = page == p ? "active" : "no"
			  dispPageNo = p+1
			  str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">'+ dispPageNo +'</a></li>'
			  //str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">next</a></li>'
			}
			params.page = --p
			clas = page == params.page ? "active" : "no"
			str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">Last</a></li>'
			
			return str
		}
		next();
	});	
}
