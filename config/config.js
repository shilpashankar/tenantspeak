var path = require('path')
var root = path.resolve(__dirname + '../..')


module.exports = {
  development: {
    root: root,
    db: 'mongodb://localhost:27017/tenantspeakdb',
    "facebookAuth" : {
        "clientID"      : "1424595254497065", // your App ID
        "clientSecret"  : "e1acec236bf84467b186292cda426c72", // your App Secret
        "callbackURL"   : 'http://localhost:3000/auth/facebook/callback'
    },

    /*'twitterAuth' : {
        'consumerKey'       : 'your-consumer-key-here',
        'consumerSecret'    : 'your-client-secret-here',
        'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    },*/

    "googleAuth" : {
        "clientID"      : "243111376356-9j4sijdocga405atj5gmsrnv6fujpfl5.apps.googleusercontent.com",
        "clientSecret"  : "AMMhjGUQVGnJjyeBP99O39kv",
        "callbackURL"   : "http://localhost:3000/auth/google/callback"
    }
  },
  //test: {
    //root: root,
    //db: 'mongodb://localhost/NaasTest'
  //},
  staging: {
    root: root,
    db: process.env.MONGOHQ_URL
  },
  test: {
    root: root,
    db: process.env.OPENSHIFT_MONGODB_DB_URL + 'test',
    "facebookAuth" : {
        "clientID"      : "1447925195497404", // your App ID
        "clientSecret"  : "c54902fe0c452f0caebf184360dda919", // your App Secret
        "callbackURL"   : 'http://test-ssapplication.rhcloud.com/auth/facebook/callback'
    },

    /*'twitterAuth' : {
        'consumerKey'       : 'your-consumer-key-here',
        'consumerSecret'    : 'your-client-secret-here',
        'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    },*/

    "googleAuth" : {
        "clientID"      : "317334930160-3laafucn30274hm74s63o0ijlk85g0rr.apps.googleusercontent.com",
        "clientSecret"  : "IMQcTxpkfOtJhMS2Bguyhl5x",
        "callbackURL"   : "http://test-ssapplication.rhcloud.com/auth/google/callback"
    }
  },
  production: {
    root: root,
    db: process.env.OPENSHIFT_MONGODB_DB_URL + 'tenantspeak',
    "facebookAuth" : {
        "clientID"      : "1424594464497144", // your App ID
        "clientSecret"  : "67385d8d313a3dc2fa92d5a216e6d2e6", // your App Secret
        "callbackURL"   : 'http://www.residentexperiences.com'
    },

    /*'twitterAuth' : {
        'consumerKey'       : 'your-consumer-key-here',
        'consumerSecret'    : 'your-client-secret-here',
        'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    },*/

    "googleAuth" : {
        "clientID"      : "618804964621-f7dotbo2r4ktrbv3nb1s3i2vj0qft2kl.apps.googleusercontent.com",
        "clientSecret"  : "cEtINjiwVkEHOYDKGNbGh2PW",
        "callbackURL"   : "http://www.residentexperiences.com"
    }
  }
}
