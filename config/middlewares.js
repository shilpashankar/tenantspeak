
/**
 * Module dependencies.
 */
 //var $ = require('jquery');
exports.requiresLogin = function (req, res, next) {
  if (req.user){
  	console.log("logged in");  	
    next();
  }
  else{
  	req.session.redirect_to = req.originalUrl;
  	console.log("not logged in");
    req.session.loadLogin = "true";
    //$("#loginModalDialog").modal('show');
    //return res.redirect('/modallogin');
    //return res.redirect('/login');
    return res.redirect('back');
  }
}

// route middleware to make sure user is logged in
function isLoggedIn(req, res, next) {

  // if user is authenticated in the session, carry on
  if (req.isAuthenticated()){
    return next();
  }

  // if they aren't redirect them to the login page
  else{
    req.session.redirect_to = req.originalUrl;
    console.log("not logged in");
    req.session.loadLogin = "true";
    //return res.redirect('/login');
    return res.redirect('back');
  }
}

/**
 * Check headers
 */

exports.checkHeaders = function (req, res, next) {
  next();
}
