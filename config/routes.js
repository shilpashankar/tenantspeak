/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var middlewares = require('./middlewares');
var passportOptions = {
  failureFlash: 'Invalid email or password.',
  failureRedirect: '/login'
}

/**
 * Route Middlewares
 */

var rl = middlewares.requiresLogin;

/**
 * Controllers
 */

var users = require('../app/controllers/users');
var userExperiences = require('../app/controllers/userExperiences');
var messages = require('../app/controllers/messages');
var home = require('../app/controllers/home');

/**
 * Expose Routes
 */

module.exports = function (app, passport) {
app.get('/', home.index);
app.get('/privacypolicy', home.privacypolicy);
app.get('/termsandconditions', home.termsandcondition);
app.get('/login', users.loadLogin);
/*app.post(
    '/login',
    passport.authenticate('local', passportOptions),
    users.login
  );*/
app.post('/login', passport.authenticate('local-login', {failureRedirect : '/login', failureFlash : true}),
	users.login);
//Facebook Login routes
app.get("/auth/facebook", passport.authenticate("facebook",{ scope : "email"}));

app.get("/auth/facebook/callback",
    passport.authenticate("facebook",{ failureRedirect: '/login'}),
    users.login
);
//Google login routes
app.get("/auth/google", passport.authenticate("google",{ scope : ['profile', 'email']}));

app.get("/auth/google/callback",
    passport.authenticate("google",{ failureRedirect: '/login'}),
    users.login
);
app.get('/logout', users.logout);

//CRUD for users (only get and post so far)
app.get('/users', users.load);
app.get('/signup', users.loadSignup);
app.post('/signup', passport.authenticate('local-signup', {failureRedirect : '/signup', failureFlash : true}),
	users.signup);
//Routes for Edit Profile functionality
app.get('/user/profile/:currentUserId',[rl], users.loadEditProfile);
app.post('/user/profile/:currentUserId',[rl], users.editProfile);
//Routes for Upload/Change Profile Photo functionality
app.get('/user/profile/photo/:currentUserId',[rl], users.loadUploadPhoto);
app.post('/user/profile/photo/:currentUserId',[rl], users.uploadPhoto);
app.post('/user/photoCrop',[rl], users.cropPhoto);
//Routes for the messaging functionality
app.get('/messages', [rl], messages.showInbox);
app.get('/messages/sent', [rl], messages.showSentItems);
app.get('/messages/saved', [rl], messages.showDrafts);
app.get('/messages/compose/:recipientId',[rl], messages.loadComposeMessage);
app.post('/messages/compose',[rl], messages.composeMessage);
app.post('/messages/saveDraft', [rl], messages.saveDraft);
app.post('/messages/reply/:messageThreadId', messages.sendReply);
app.delete('/messages/:messageThreadId', messages.deleteMessageThread);
app.post('/messages/messageStatus/:messageId', messages.updateMessageReadStatus);
app.post('/messages/sendFromDrafts/:messageThreadId', messages.sendMessageFromDrafts);
//Send email functionality
app.get('/users/emailTenant', users.emailTenant);
//console.log('am in routes.js');
/*if(typeof req === 'undefined'){
	console.log('am yet to receive a request moron!!');
}
else{
	console.log(req.originalUrl);
}*/
//CRUD for userExperiences
app.get('/userExperience/new', [rl], userExperiences.new);
app.post('/userExperience/:currentUserId', [rl], userExperiences.create);
app.get('/userExperience/search', userExperiences.loadSearch);
app.get('/userExperience/results',[rl], userExperiences.load);
app.get('/userExperience/myExperiences', [rl],  userExperiences.showCurrUserExp);
app.get('/userExperience/:userExperienceId', [rl],  userExperiences.loadById);
app.get('/userExperience/edit/:userExperienceId', [rl], userExperiences.loadForEdit);
app.post('/userExperience/edit/:userExperienceId', [rl], userExperiences.modify);
app.delete('/userExperience/:selectedUserExpId', [rl], userExperiences.remove);

}