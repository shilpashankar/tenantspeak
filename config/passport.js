var mongoose = require('mongoose')
  , LocalStrategy = require('passport-local').Strategy
  //Including code for Facebook authorization.
  , FacebookStrategy = require('passport-facebook').Strategy
  , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
  , User = mongoose.model('user')  

module.exports = function (passport, config) {
  // require('./initializer')

  // serialize sessions
  passport.serializeUser(function(user, done) {
    done(null, user.id)
  })

  passport.deserializeUser(function(id, done) {
    User.findOne({ _id: id }, function (err, user) {
      done(err, user)
    })
  })
  
  /*passport.deserializeUser(function(id, done) {
    FbUsers.findById(id,function(err,user){
    	console.log("FB find " + user);
        if(err){
        	console.log("an error occured " + err);
        	done(err);
        }
        if(user){
        	console.log("FB find " + user);
            done(null,user);
        }
        else{
            User.findOne({ _id: id }, function (err, user) {
                if(err) done(err);
                console.log('User object is : ' + user) ;                
                done(null,user);
            });
        }
    });
  });*/

  // Local signup
  passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {    	
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            // check to see if theres already a user with that email
            if (user) {                
                return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            }                  
            else {
                //Checking if the username is already been taken.
                User.findOne({'local.username' : req.body.username}, function(err, user){
                    if (err)
                        return done(err);
                    if(user){
                        return done(null, false, req.flash('signupMessage', 'That username is already taken.'));
                    }
                    else{
                            // if there is no user with that email and username
                            // create the user                        
                            var newUser            = new User();

                            // set the user's local credentials
                            newUser.local.email    = email;
                            newUser.local.username = req.body.username;
                            newUser.local.password = newUser.hashPassword(password);
                            console.log(done);

                            // save the user
                            newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            });
                    }
                })
                
            }

        });    

        });

    }));

	passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        //console.log("I am inside the passport local login method! 00" + email);
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error before anything else
            //console.log("I am inside the passport local login method! 11" + email);
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user){
                //req.session.loginError = 'No user found.';
                //req.flash('loginMessage', 'No user found.');
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
            }

            // if the user is found but the password is wrong
            if (!user.validPassword(password)){
                //req.session.loginError = 'Oops! Wrong password.';
                req.flash('loginMessage', 'Oops! Wrong password.');
                console.log('inside the passport method : ' + req.flash('loginMessage'));
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
            }

            // all is well, return successful user
            return done(null, user);
        });

    }));
  // use local strategy
  /*passport.use(new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password'
    },
    function(username, password, done) {
      User.findOne({ username: username }, function (err, user) {
        if (err) { 
        	console.log("Oops! Error occured!! " + err);
        	return done(err); 
        }
        if (!user) {
          console.log("unknown user");
          return done(null, false, { message: 'No user found' });
        }
        if (!user.validPassword(password)) {          
          return done(null, false, { message: 'Invalid password' });
        }
        return done(null, user);
      });
    }
  ));*/

  //Facebook Strategy.
  passport.use(new FacebookStrategy({

        clientID        : config.facebookAuth.clientID,
        clientSecret    : config.facebookAuth.clientSecret,
        callbackURL     : config.facebookAuth.callbackURL 

    },

    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // find the user in the database based on their facebook id
            User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                console.log("am inside facebook authentication");
            	console.log(user);
            	console.log(done);
                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);

                // if the user is found, then log them in
                if (user) {
                    console.log("facebook authentication : " + user);
                    return done(null, user); // user found, return that user
                } else {
                    // if there is no user found with that facebook id, create them
                    var newUser = new User();
                    console.log("newUser is: " + newUser);
                    console.log("profile is: " + profile);
                    // set all of the facebook information in our user model
                    newUser.facebook.id = profile.id; // set the users facebook id                   
                    newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
                    newUser.facebook.username = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                    newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                    console.log("newUser is: " + newUser);
                    // save our user to the database
                    newUser.save(function(err) {
                        if (err){
                        	console.log(err);
                            throw err;
                        }

                        // if successful, return the new user
                        return done(null, newUser);
                    });
                }

            });
        });

    }));
	
	//Google Strategy.
	passport.use(new GoogleStrategy({

        clientID        : config.googleAuth.clientID,
        clientSecret    : config.googleAuth.clientSecret,
        callbackURL     : config.googleAuth.callbackURL
    },
    function(token, refreshToken, profile, done) {

        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function() {

            // try to find the user based on their google id
            User.findOne({ 'google.id' : profile.id }, function(err, user) {
                console.log("am inside google authentication");
                if (err)
                    return done(err);

                if (user) {

                    // if a user is found, log them in
                    console.log("google authentication : " + user);
                    return done(null, user);
                } else {
                    // if the user isnt in our database, create a new user
                    var newUser          = new User();                    
                    // set all of the relevant information
                    newUser.google.id    = profile.id;
                    newUser.google.token = token;
                    newUser.google.username  = profile.displayName;
                    newUser.google.email = profile.emails[0].value; // pull the first email
                    console.log("New user : " + newUser);
                    // save the user
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });

    }));
  /*passport.use(new FacebookStrategy({
    clientID: "1424595254497065",
    clientSecret: "e1acec236bf84467b186292cda426c72",
    callbackURL: "http://localhost:3000/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    FbUsers.findOne({fbId : profile.id}, function(err, oldUser){
        if(oldUser){
            done(null,oldUser);
        }
        else{
	            var newUser = new FbUsers({
	                fbId : profile.id ,
	                email : profile.emails[0].value,
	                name : profile.displayName
	            }).save(function(err,newUser){
	                if(err) throw err;
	                done(null, newUser);
	            });
        	}
    	});
  	}));*/
}