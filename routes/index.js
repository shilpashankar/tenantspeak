var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

/*GET users. */
router.get('/users', function(req, res){
	var db = req.db;
	var users = db.model('user').find({}, {}, function(err,doc){
		res.send(doc);
	});
});

/*GET user experiences. */
router.get('/userExperience', function(req, res){
	var db = req.db;
	var getUserExpCriteria = {};
	if(req.headers['locality'] !== undefined){		
		getUserExpCriteria.locality = req.headers['locality'];		
	}
	if(req.headers['city'] !== undefined){		
		getUserExpCriteria.city = req.headers['city'];		
	}
	if(req.headers['type'] !== undefined){		
		getUserExpCriteria.type = req.headers['type'];		
	}
	var users = db.model('userExperience').find(getUserExpCriteria, {}, function(err,doc){
		res.send(doc);
	});
});

/*GET user experiences. */
router.get('/userExperience/:userExperienceId', function(req, res){
	var db = req.db;
	var getUserExpCriteria = {
		_id = req.params.userExperienceId
	};	
	var users = db.model('userExperience').find(getUserExpCriteria, {}, function(err,doc){
		res.send(doc);
	});
});

router.post('/userExperience/:currentUserId', function(req, res){
	var db = req.db;
	var inputExperience = {
		"locality" : req.headers['locality'],
		"city" : req.headers['city'],
		"type" : req.headers['type'],
		"floor" : req.headers['floor'],
		"liftService" : req.headers['lift'],
		"propertyCondition" : req.headers['propcond'],
		"carPark" : req.headers['carpark'],
		"storesNearby" : req.headers['stores'],
		"entertainmentVenues" : req.headers['entertain'],
		"transportConnectivity" : req.headers['transport'],
		"waterSupply" :req.headers['watersupp'],
		"powerSupply" : req.headers['powersupp'],
		"drainPipesQuality" : req.headers['drainqual'],
		"electricPlugsSwitchesCondition" : req.headers['eleccond'],
		"withinCompoundAmenities" : req.headers['incompamen'],
		"trafficCongestion" : req.headers['traffic'],
		"defectsDetected" : req.headers['defects'],
		"comments" : req.headers['comments'],
		"rating" : req.headers['rating'],
		"userid" : currentUserId
	};
	console.log(inputExperience);
	var userExperience = db.model('userExperience');
	userExperience.create(inputExperience, function(err,inputExperience){
		if(err){
			res.send("A problem occurred while inserting your data");
		}
		else{
			res.send(inputExperience);
		}
	});
});

/* DELETE user experience */
router.delete('/userExperience/:selectedUserExpId', function(req, res){
	var db = req.db;
	var userExperience = db.model('userExperience');
	var expIdToBeDeleted = req.params.selectedUserExpId;
	userExperience.findByIdAndRemove(expIdToBeDeleted,function(err,doc){
		if(err){
			console.log(err); 
			res.send("The record could not be deleted!!");			
		}
		else{
			res.send(doc);
		}
	});
});

/* UPDATE user experience */
router.put('/userExperience/:selectedUserExpId', function(req, res){
	var db = req.db;
	var userExperience = db.model('userExperience');
	var modifiedExperience = {
		"locality" : req.headers['locality'],
		"city" : req.headers['city'],
		"type" : req.headers['type'],
		"floor" : req.headers['floor'],
		"liftService" : req.headers['lift'],
		"propertyCondition" : req.headers['propcond'],
		"carPark" : req.headers['carpark'],
		"storesNearby" : req.headers['stores'],
		"entertainmentVenues" : req.headers['entertain'],
		"transportConnectivity" : req.headers['transport'],
		"waterSupply" :req.headers['watersupp'],
		"powerSupply" : req.headers['powersupp'],
		"drainPipesQuality" : req.headers['drainqual'],
		"electricPlugsSwitchesCondition" : req.headers['eleccond'],
		"withinCompoundAmenities" : req.headers['incompamen'],
		"trafficCongestion" : req.headers['traffic'],
		"defectsDetected" : req.headers['defects'],
		"comments" : req.headers['comments'],
		"rating" : req.headers['rating'],
		"userid" : req.headers['userid']
	};
	console.log(req.params.selectedUserExpId);
	userExperience.findByIdAndUpdate(req.params.selectedUserExpId, modifiedExperience, function(err, doc){
		if(err){
			console.log(err);
			res.send("An error occured while trying to fetch the requested data");
		}
		else{
			res.send(doc);
		}
	});
});

//GET method for sending email to the tenant.
router.get('/users/emailTenant', function(req, res){
	// create reusable transporter object using SMTP transport
	var transporter = nodemailer.createTransport({
	    service: 'Gmail',
	    auth: {
	        user: 'shilp.shankar@gmail.com',
	        pass: 'shil0804'
	    }
	});

	//Obtain and set the logged in user's email ID and the recipient's email id.
	var recipientEmail = req.headers['recipientemail'];
	var emailSubject = req.headers['emailsubject'];
	var emailContent = req.headers['emailcontent'];
	var senderEmail = 'shilp.shankar@gmail.com' //change this later to retrieve the email id of the logged in user from the session.

	// NB! No need to recreate the transporter object. You can use
	// the same transporter object for all e-mails

	// setup e-mail data with unicode symbols
	var mailOptions = {
	    from: senderEmail, // sender address
	    to: recipientEmail, // list of receivers
	    subject: 'ResidentExperiences : ' + emailSubject, // Subject line
	    text: emailContent, // plaintext body	   
	};

	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        console.log(error);
	    }else{
	        console.log('Message sent: ' + info.response);
	        res.send(info);
	    }
	});
});

module.exports = router;
