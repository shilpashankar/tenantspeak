var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var env = process.env.NODE_ENV || 'development';
var config = require('./config/config')[env];
var passport = require('passport');
var flash = require('connect-flash');
var multer = require('multer');
//require the file system library.
var fs = require('fs');
//require mongodb, mongoose and create a connection object.
var mongoose = require('mongoose');
var db = mongoose.connect(config.db);
//require nodemailer
var nodemailer = require('nodemailer');

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 3000;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var paginate = require('express-paginate');

//Load all the files in the models directory
fs.readdirSync(__dirname + '/app/models').forEach(function(filename){
    if(~filename.indexOf('.js')){
        require(__dirname + '/app/models/' + filename);
    }
});

//import passport config file.
require('./config/passport')(passport, config);

var app = express();

//add the db connection in the request object  to be accessible in other folders.
app.use(function(req,res,next){
    req.db = db;
    next();
});

// Bootstrap application settings
require('./config/express')(app, config, passport);

//include pagination middleware
app.use(paginate.middleware(10,50));
//Bootstrap routes
require('./config/routes')(app, passport);


//Error handler.
/// catch 404 and forward to error handler
	app.use(function(req, res, next) {
	    var err = new Error('Not Found');
	    //console.log(req.originalUrl);	    	  
	    err.status = 404;
	    next(err);
	});

	/// error handlers

	// development error handler
	
	// will print stacktrace
	if (app.get('env') === 'development') {
	    app.use(function(err, req, res, next) {
	        res.status(err.status || 500);
	        res.render('error', {
	            message: err.message,
	            error: err
	        });
	    });
	}

	// production error handler
	// no stacktraces leaked to user
	app.use(function(err, req, res, next) {
	    res.status(err.status || 500);	    
	    res.render('error', {
	        message: err.message,
	        error: {}
	    });
	});

app.listen(server_port, server_ip_address, function () {
  console.log( "Listening on " + server_ip_address + ", server_port " + server_port )
});

module.exports = app;
